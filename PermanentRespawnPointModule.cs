﻿using System;
using Monocle;
using Microsoft.Xna.Framework;

namespace Celeste.Mod.PermanentRespawnPoint
{
    public class PermanentRespawnPointModule : EverestModule
    {
        public static PermanentRespawnPointModule Instance;
        public string SpawnLevel { get; private set; }
        public bool SpawnLevelEnabled{ get; private set; }
        public PermanentRespawnPointModule()
        {
            Instance = this;
        }

        private PlayerDeadBody OnPlayerDie(On.Celeste.Player.orig_Die orig,Player self,Vector2 direction, bool evenIfInvincible = false, bool registerDeathInStats = true)
        {
            PlayerDeadBody body = orig(self,direction,evenIfInvincible,registerDeathInStats);
            if(!SpawnLevelEnabled)
            {
                return body;
            }

            if (!(Engine.Scene is Level level))
            {
                return body;
            }

                body.DeathAction = () =>
                {
                    LevelExit exit = new LevelExit(LevelExit.Mode.GoldenBerryRestart, level.Session)
                    {
                        GoldenStrawberryEntryLevel = SpawnLevel
                    };
                    Engine.Scene = exit;
                };
            return body;

        }

        public void SetRespawnLevel(String spawnLevel)
        {
            SpawnLevelEnabled = true;
            SpawnLevel = spawnLevel;
        }

        public void DisableRespawn()
        {
            SpawnLevelEnabled = false;
        }

        private void OnLevelExit(Level level, LevelExit exit, LevelExit.Mode mode, Session session, HiresSnow snow)
        {
            if (mode != LevelExit.Mode.GoldenBerryRestart && mode != LevelExit.Mode.Restart && mode != LevelExit.Mode.CompletedInterlude)
            {
                DisableRespawn();
            }
        }


        public override Type SettingsType => null;

        public override void Load(){
            On.Celeste.Player.Die += OnPlayerDie;
            Everest.Events.Level.OnExit += OnLevelExit;
        }

        public override void Unload()
        { 
            On.Celeste.Player.Die -= OnPlayerDie;
            Everest.Events.Level.OnExit -= OnLevelExit;
        }
    }
}
