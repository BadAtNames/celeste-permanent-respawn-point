﻿using Monocle;

namespace Celeste.Mod.PermanentRespawnPoint
{
    public static class PermanentRespawnPointCommands
        {
        [Command("set_respawn", "Sets the current room as the permanent respawn room")]
        private static void SetRespawn()
        {

            if (!(Engine.Scene is Level level))
            {
                return;
            }
            PermanentRespawnPointModule.Instance.SetRespawnLevel(level.Session.Level);
        }

        [Command("unset_respawn", "Disables the permanent respawn room")]
        private static void UnsetRespawn()
        {
            PermanentRespawnPointModule.Instance.DisableRespawn();
        }
    }
}
