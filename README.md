This mod allows you to set any room as the room to respawn in after you die. I've found it useful when grinding golden berries.

Use the `set-respawn` and `unset-respawn` commands in developer console to activate and deactivate the respawn point.

You need everest to use this mod. For building you need to [setup your c# assembly references](https://github.com/EverestAPI/Resources/wiki/Your-First-Code-Mod#project-setup)
